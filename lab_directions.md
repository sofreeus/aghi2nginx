# Lab Directions

Inside VirtualBox  import Centos7 image provided “Centos7-Student”

You can perform these steps via Terminal or ssh into the box

--- INSTALL NGINX ---

```bash
sudo yum install epel-release
sudo yum -y install nginx
sudo systemctl start nginx
```

---

Handy command for reloading NGINX config after you make changes: (not now, later)

```bash
sudo nginx -t && sudo nginx -s reload
```

(-t checks config syntax,  -s reloads)


--- DOCKER ---

Check that docker is running:

```bash
docker -v
```

--- Create docker containers ---

    • Under user directory – create webservers directory

```bash
mkdir webservers
cd webservers
docker pull nginx
```

^ will download NGINX image from DockerHub

OR

Make a file called “Dockerfile”  add one line

```
FROM nginx
```

You can run Dockerfile using this command:

```bash
docker build -t <name your container> .
```

(call first image webserver)

Check your container is running

`docker ps` or `docker ps -a` to see ALL containers

Install VIM into webserver and commit to new image

```bash
apt-get update
apt-get install vim
```

```bash
docker commit <name-or-hash> webserver:version2
```

Build all new images from webserver:version2

```bash
docker run --detach -p 8080:8080 --name mywebserver8080 webserver:version2
docker run --detach -p 8081:8081 --name mywebserver8181 webserver:version2
docker run --detach -p 8082:8082 --name mywebserver8282 webserver:version2
```

Other useful docker commands:

Login to container:

```bash
docker exec -it <containername> /bin/bash
```

`docker stop` <--stops container
`docker rm <image name or id>` <-- removes container after you stopped it
`docker rmi` <-- remove an image

`docker ps` <-- shows running containers
`docker ps -a` <-- shows all containers
`docker images` <-- shows all images
`docker inspect` <-- (looks at details including ip address)

look at open ports on Virtual Servers

```bash
sudo lsof -i -P -n | grep LISTEN 
sudo netstat -tulpn | grep LISTEN
```

Edit NGINX Config

Configs in NGINX:
`/etc/nginx/nginx.conf`
`/etc/nginx/nginx.conf/conf.d/default.conf`

User content NGINX:
`/usr/share/nginx/html`
